package invoices

// Organisation represents an organisation, for example the invoice issuer or the client.
type Organisation struct {
	Name      string
	Reference string
	Address   string
	PostCode  string
	City      string
	Website   string
	Email     string
	EmailCC   string
	Telephone string
}
