package main

import (
	"encoding/json"
	"io/ioutil"
	"net"
	"net/http"
	"time"

	"gitlab.com/bazzz/invoices"
	"gitlab.com/bazzz/log"
)

const host = ""
const port = "9100"

func main() {
	log.Connect()
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			return
		}
		defer r.Body.Close()
		data, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Error(err)
			return
		}
		invoiceData := Invoice{}
		if err := json.Unmarshal(data, &invoiceData); err != nil {
			log.Error(err)
			return
		}
		invoice := invoices.New(invoiceData.Identification)
		switch invoiceData.Language {
		case "eng":
			invoice.Labels = invoices.LabelsEnglish
		case "nld":
			invoice.Labels = invoices.LabelsNederlands
		}
		if invoiceData.DateFormat != "" {
			invoice.Style.DateFormat = invoiceData.DateFormat
		}

		invoice.Logo = invoiceData.Logo
		invoice.Sender = invoiceData.Sender
		invoice.Recipient = invoiceData.Recipient
		invoice.BankAccount = invoiceData.BankAccount
		invoice.Taxnumber = invoiceData.Taxnumber
		invoice.InvoiceDate = invoiceData.InvoiceDate
		invoice.FromDate = invoiceData.FromDate
		invoice.ToDate = invoiceData.ToDate
		invoice.TotalWithoutTax = invoiceData.TotalWithoutTax
		invoice.TotalTax = invoiceData.TotalTax
		invoice.Total = invoiceData.Total

		for _, header := range invoiceData.Headers {
			align := invoices.AlignDefault
			switch header.Alignment {
			case "left":
				align = invoices.AlignLeft
			case "center":
				align = invoices.AlignCenter
			case "right":
				align = invoices.AlignRight
			}
			invoice.AddItemHeader(header.WidthPercentage, header.Text, align, invoices.BorderBottom)
		}

		for _, item := range invoiceData.Items {
			invoice.AddItem(item)
		}

		w.Header().Set("Content-Type", "application/pdf")
		if err := invoice.Write(w); err != nil {
			log.Error(err)
			return
		}
	})
	log.Fatal(http.ListenAndServe(net.JoinHostPort(host, port), nil))
}

type Invoice struct {
	Identification  string
	Language        string
	Logo            string
	DateFormat      string
	Sender          invoices.Organisation
	Recipient       invoices.Organisation
	PaymentSpan     int
	BankAccount     string
	Taxnumber       string
	InvoiceDate     time.Time
	FromDate        time.Time
	ToDate          time.Time
	TotalWithoutTax string
	TotalTax        string
	Total           string
	Headers         []Header
	Items           [][]string
}

type Header struct {
	WidthPercentage int
	Text            string
	Alignment       string
}
