package invoices

// Labels represents the collection of simple text labels in a certain language used in the invoice.
type Labels struct {
	BankAccount     string
	HeaderText      string
	Identification  string
	InvoiceDate     string
	PaymentSpan     string
	PaymentSpanUnit string
	Period          string
	PeriodFromTo    string
	Taxnumber       string
	Total           string
	TotalTax        string
	TotalWithoutTax string
}

// LabelsEnglish represents the labels in English.
var LabelsEnglish = Labels{
	BankAccount:     "Bank account",
	HeaderText:      "INVOICE",
	Identification:  "Invoice number",
	InvoiceDate:     "Invoice date",
	PaymentSpan:     "Please pay within",
	PaymentSpanUnit: "days",
	Period:          "Period",
	PeriodFromTo:    "-",
	Taxnumber:       "VAT registration",
	Total:           "Total",
	TotalTax:        "VAT",
	TotalWithoutTax: "Total without VAT",
}

// LabelsNederlands represents the labels in Dutch.
var LabelsNederlands = Labels{
	BankAccount:     "Bankrekening",
	HeaderText:      "FACTUUR",
	Identification:  "Factuurnummer",
	InvoiceDate:     "Factuurdatum",
	PaymentSpan:     "Gelieve te betalen binnen",
	PaymentSpanUnit: "dagen",
	Period:          "Periode",
	PeriodFromTo:    "t/m",
	Taxnumber:       "BTW-nummer",
	Total:           "Totaal",
	TotalTax:        "BTW",
	TotalWithoutTax: "Totaal excl. BTW",
}
