package invoices

type alignment string

const (
	// AlignDefault does not express any text alignment.
	AlignDefault alignment = ""

	// AlignLeft expresses that text should be aligned left.
	AlignLeft alignment = "L"

	// AlignCenter expresses that text should be aligned center.
	AlignCenter alignment = "C"

	// AlignRight expresses that text should be aligned right.
	AlignRight alignment = "R"
)
