package invoices

import (
	"path/filepath"
	"strings"
)

// Item represents an invoice line for a sold product or service.
type Item []string

// GetAsAssetName returns the asset full file path for the element of this item as the provided index.
func (i Item) GetAsAssetName(index int) string {
	prefix := "image::"
	if strings.HasPrefix(i[index], prefix) {
		return i[index][len(prefix):]
	}
	return i[index]
}

// GetAsAssetFileBase returns the asset filename for the element of this item as the provided index.
func (i Item) GetAsAssetFileBase(index int) string {
	name := strings.TrimPrefix(i[index], "image::")
	return filepath.Base(name)
}
