package invoices

// Color represents a color, expressed in RGB components (0 - 255).
type Color struct {
	Red   int
	Green int
	Blue  int
}
