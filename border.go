package invoices

type border string

const (
	// BorderNone indicates that no border should be added.
	BorderNone border = ""

	// BorderFull indicates that a border should be added to every side.
	BorderFull border = "1"

	// BorderBottom indicates that a border should be added only to the bottem.
	BorderBottom border = "B"
)
