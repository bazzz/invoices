package invoices

// Font represents a text font.
type Font struct {
	Name  string
	Style string
	Size  float64
	Color *Color
}
