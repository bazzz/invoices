package invoices

// Style represents the settings for the visual style of the invoice.
type Style struct {
	DateFormat  string
	Font        *Font
	ItemBorders border
	ItemFont    *Font
	ItemSubFont *Font
}

// StyleDefault represents a default visual style.
var StyleDefault = Style{
	DateFormat:  "yyyy-MM-dd",
	Font:        &Font{"Arial", "", 11, &Color{0, 0, 0}},
	ItemBorders: BorderNone,
	ItemFont:    &Font{"Arial", "", 11, &Color{0, 0, 0}},
	ItemSubFont: &Font{"Arial", "I", 8, &Color{100, 100, 100}},
}
