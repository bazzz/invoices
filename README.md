# invoices

Package invoices generates invoice pdf files.

## Issues

This package probably still has quite a number of issues because it was extracted from an application that had few ways to configure how the invoice would look. An example is the inability to change language beyond English and Dutch which I sort of hacked in. But also the layout is not much configurable. It sort of works for now.

## Images
Images need to be sent as base64 jgp/png/gif. This package uses my other package `imagebase64` to decode the encoded image. You can use the same package to encode your image as to ensure everything works well together.

## Example
The file main.go provides an example implementation of how you can use this package to host a micro service to generate invoices. Of course you can also simply import the packe in your own application bypassing main.go altogether. In main.go you can remove the custom logger and use the default log package if you prefer. The invoices package itself does not log anything and therefore does not refer to any logger at all.