package invoices

import (
	"bytes"
	"io"
	"strconv"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/jung-kurt/gofpdf"
	"gitlab.com/bazzz/imagebase64"
)

// Invoice represents an invoice.
type Invoice struct {
	Sender          Organisation
	Recipient       Organisation
	Labels          Labels
	Style           Style
	Logo            string
	PaymentSpan     int
	BankAccount     string
	Taxnumber       string
	Identification  string
	InvoiceDate     time.Time
	FromDate        time.Time
	ToDate          time.Time
	TotalWithoutTax string
	TotalTax        string
	Total           string
	itemHeaders     []header
	itemData        []Item
	pdf             *gofpdf.Fpdf
}

// NewInvoice is the constructor for Invoice.
func New(identification string) *Invoice {
	defaultDate := time.Now()
	invoice := Invoice{
		Labels:         LabelsEnglish,
		Style:          StyleDefault,
		Identification: identification,
		FromDate:       defaultDate,
		ToDate:         defaultDate,
		InvoiceDate:    defaultDate,
		pdf:            gofpdf.New("P", "mm", "A4", ""),
		itemHeaders:    make([]header, 0),
		itemData:       make([]Item, 0),
	}
	return &invoice
}

// Items returns the item collection of this invoice.
func (i *Invoice) Items() []Item {
	return i.itemData
}

// AddItemHeader adds an items header to this invoice.
func (i *Invoice) AddItemHeader(widthPercentage int, text string, textAlignment alignment, borderSetting border) {
	newHeader := header{
		block:           block{Text: text},
		WidthPercentage: widthPercentage,
	}
	newHeader.SetTextAlignment(textAlignment)
	newHeader.SetBorder(borderSetting)
	i.itemHeaders = append(i.itemHeaders, newHeader)
}

// AddItem adds an item to this invoice.
func (i *Invoice) AddItem(item []string) {
	i.itemData = append(i.itemData, item)
}

// Write generates the pdf invoice document and writes it to w.
func (i *Invoice) Write(w io.Writer) error {
	i.generate()
	return i.pdf.Output(w)
}

// WriteToFile generates the pdf invoice document and writes it to outFile.
func (i *Invoice) WriteToFile(outFile string) error {
	i.generate()
	return i.pdf.OutputFileAndClose(outFile)
}

func (i *Invoice) registerImage(base64data string) (options gofpdf.ImageOptions, name string, err error) {
	data, contentType, err := imagebase64.Decode(base64data)
	if err != nil {
		return
	}
	switch contentType {
	case "image/jpg":
		options.ImageType = "jpg"
	case "image/png":
		options.ImageType = "png"
		options.ReadDpi = true
	case "image/gif":
		options.ImageType = "gif"
	}
	name = uuid.New().String()
	i.pdf.RegisterImageOptionsReader(name, options, bytes.NewBuffer(data))
	return
}

func (i *Invoice) generate() error {
	leftMargin, topMargin, bottomMargin := 10.0, 10.0, 10.0
	i.pdf.SetMargins(leftMargin, topMargin, bottomMargin)
	i.pdf.AddPage()
	i.setFont(i.Style.Font)
	if strings.HasPrefix(i.Logo, "data:image") {
		options, name, err := i.registerImage(i.Logo)
		if err != nil {
			return err
		}
		i.pdf.ImageOptions(name, leftMargin, 0, 0, 25, true, options, 0, "")
	}
	i.renderVerticalSpace(5)

	width, _ := i.pdf.GetPageSize()
	width = width - leftMargin - leftMargin

	senderInfo := i.getOrganisationInfo(i.Sender)
	bankInfo := i.getBankInfo()
	senderAndBankInfo := i.mergeTwoInfos(senderInfo, bankInfo)
	i.renderTwoColumns(senderAndBankInfo, width/3*2, width/3)

	i.pdf.Ln(-1)

	recipientInfo := i.getOrganisationInfo(i.Recipient)
	invoiceInfo := i.getInvoiceInfo()
	recipientAndInvoiceInfo := i.mergeTwoInfos(recipientInfo, invoiceInfo)
	i.renderTwoColumns(recipientAndInvoiceInfo, width/3*2, width/3)

	i.pdf.Ln(-1)

	headerFont := &Font{i.Style.Font.Name, i.Style.Font.Style, i.Style.Font.Size * 2, nil}
	i.setFont(headerFont)
	i.pdf.Cell(0, headerFont.Size, i.Labels.HeaderText)
	i.setFont(i.Style.Font)
	i.pdf.Ln(-1)

	verticalSpace := 3.0
	i.renderItems(width, verticalSpace)
	i.renderVerticalSpace(verticalSpace)
	i.renderTotals(width)
	return nil
}

func (i *Invoice) setFont(newFont *Font) {
	fontName := i.Style.Font.Name
	if newFont.Name != "" && newFont.Name != fontName {
		fontName = newFont.Name
	}
	fontStyle := i.Style.Font.Style
	if newFont.Style != "" && newFont.Style != fontStyle {
		fontStyle = newFont.Style
	}
	fontSize := i.Style.Font.Size
	if newFont.Size != 0 && newFont.Size != fontSize {
		fontSize = newFont.Size
	}
	i.pdf.SetFont(fontName, fontStyle, fontSize)

	color := i.Style.Font.Color
	if newFont.Color != nil {
		color = newFont.Color
	}
	i.pdf.SetTextColor(color.Red, color.Green, color.Blue)
}

func (i *Invoice) renderTwoColumns(data []block, leftColumnWidth float64, rightColumnWidth float64) {
	for index, cell := range data {
		width := rightColumnWidth
		if index%2 == 0 {
			width = leftColumnWidth
		}
		height := (i.Style.Font.Size / 2) - 1
		if cell.Font != nil {
			i.setFont(cell.Font)
			if cell.Font.Size > 0 {
				height = cell.Font.Size / 2
			}
		}
		i.pdf.CellFormat(width, height, cell.Text, string(cell.Border()), 0, string(cell.TextAlignment()), false, 0, "")
		if index%2 == 1 {
			i.pdf.Ln(-1)
		}
		i.pdf.SetFont(i.Style.Font.Name, i.Style.Font.Style, i.Style.Font.Size)
		i.pdf.SetTextColor(0, 0, 0)
	}
}

func (i *Invoice) mergeTwoInfos(a []block, b []block) []block {
	if len(a) > len(b) {
		for i := len(b); i < len(a); i++ {
			b = append(b, block{})
		}
	} else {
		for i := len(a); i < len(b); i++ {
			a = append(a, block{})
		}
	}
	info := make([]block, 0)
	for i, item := range a {
		info = append(info, item)
		info = append(info, b[i])
	}
	return info
}

func (i *Invoice) getOrganisationInfo(organisation Organisation) []block {
	organisationInfo := make([]block, 0)
	var cell block

	if organisation.Name != "" {
		cell = block{}
		cell.Text = organisation.Name
		organisationInfo = append(organisationInfo, cell)
	}

	if organisation.Reference != "" {
		cell = block{}
		cell.Text = organisation.Reference
		organisationInfo = append(organisationInfo, cell)
	}

	if organisation.Address != "" {
		cell = block{}
		cell.Text = organisation.Address
		organisationInfo = append(organisationInfo, cell)
	}

	if organisation.PostCode != "" || organisation.City != "" {
		cell = block{}
		cell.Text = organisation.PostCode
		cell.Text += " "
		cell.Text += organisation.City
		organisationInfo = append(organisationInfo, cell)
	}

	if organisation.Website != "" {
		cell = block{}
		cell.Text = organisation.Website
		organisationInfo = append(organisationInfo, cell)
	}

	if organisation.Email != "" {
		cell = block{}
		cell.Text = organisation.Email
		organisationInfo = append(organisationInfo, cell)
	}

	if organisation.EmailCC != "" {
		cell = block{}
		cell.Text = organisation.EmailCC
		organisationInfo = append(organisationInfo, cell)
	}

	if organisation.Telephone != "" {
		cell = block{}
		cell.Text = organisation.Telephone
		organisationInfo = append(organisationInfo, cell)
	}

	return organisationInfo
}

func (i *Invoice) getBankInfo() []block {
	bankInfo := make([]block, 0)
	var cell block

	if i.PaymentSpan > 0 {
		cell = block{}
		cell.Font = &Font{"", "I", 0, &Color{100, 100, 100}}
		cell.Text = i.Labels.PaymentSpan
		bankInfo = append(bankInfo, cell)

		cell = block{}
		cell.Text = strconv.Itoa(i.PaymentSpan) + " " + i.Labels.PaymentSpanUnit
		bankInfo = append(bankInfo, cell)
	}

	cell = block{}
	cell.Font = &Font{"", "I", 0, &Color{100, 100, 100}}
	cell.Text = i.Labels.BankAccount
	bankInfo = append(bankInfo, cell)

	cell = block{}
	cell.Text = i.BankAccount
	bankInfo = append(bankInfo, cell)

	cell = block{}
	cell.Font = &Font{"", "I", 0, &Color{100, 100, 100}}
	cell.Text = i.Labels.Taxnumber
	bankInfo = append(bankInfo, cell)

	cell = block{}
	cell.Text = i.Taxnumber
	bankInfo = append(bankInfo, cell)

	return bankInfo
}

func (i *Invoice) getInvoiceInfo() []block {
	invoiceInfo := make([]block, 0)

	cell := block{}
	cell.Font = &Font{"", "I", 0, &Color{100, 100, 100}}
	cell.Text = i.Labels.Identification
	invoiceInfo = append(invoiceInfo, cell)

	cell = block{}
	cell.Text = i.Identification
	invoiceInfo = append(invoiceInfo, cell)

	cell = block{}
	cell.Text = i.Labels.InvoiceDate
	cell.Font = &Font{"", "I", 0, &Color{100, 100, 100}}
	invoiceInfo = append(invoiceInfo, cell)

	cell = block{}
	cell.Text = i.InvoiceDate.Format(i.getDateFormat())
	invoiceInfo = append(invoiceInfo, cell)

	if i.FromDate != i.ToDate {
		cell = block{}
		cell.Text = i.Labels.Period
		cell.Font = &Font{"", "I", 0, &Color{100, 100, 100}}
		invoiceInfo = append(invoiceInfo, cell)

		cell = block{}
		cell.Text = i.FromDate.Format(i.getDateFormat()) + " " + i.Labels.PeriodFromTo + " " + i.ToDate.Format(i.getDateFormat())
		invoiceInfo = append(invoiceInfo, cell)
	}

	return invoiceInfo
}

func (i *Invoice) renderItems(pageWidth float64, verticalSpace float64) {
	// 	Header
	for _, header := range i.itemHeaders {
		columnWidth := pageWidth / 100 * float64(header.WidthPercentage)
		i.pdf.CellFormat(columnWidth, 7, header.Text, string(header.Border()), 0, string(header.TextAlignment()), false, 0, "")
	}

	i.renderVerticalSpace(verticalSpace)

	// Data
	for _, c := range i.itemData {
		lineHeight := 6.0
		totalWidth := i.pdf.GetX()
		for index, text := range c {
			columnWidth := pageWidth / 100 * float64(i.itemHeaders[index].WidthPercentage)
			if strings.HasPrefix(text, "data:image") {
				options, name, err := i.registerImage(text)
				if err != nil {
					continue
				}
				size := columnWidth / 5 * 4
				if size > lineHeight {
					lineHeight = size
				}
				i.pdf.SetX(totalWidth)
				i.pdf.ImageOptions(name, -1, i.pdf.GetY(), size, size, false, options, 0, "")
			} else {
				text = i.getSymbolReplacedText(text)
				y := i.pdf.GetY()
				lines := strings.Split(text, "\n")
				dividedLineHeight := lineHeight / float64(len(lines))
				firstLineDone := false
				for _, line := range lines {
					i.pdf.SetX(totalWidth)
					if firstLineDone {
						i.setFont(i.Style.ItemSubFont)
					}
					i.pdf.CellFormat(columnWidth, dividedLineHeight, line, string(i.Style.ItemBorders), 0, string(i.itemHeaders[index].TextAlignment()), false, 0, "")
					if firstLineDone {
						i.setFont(i.Style.ItemFont)
					}
					i.pdf.SetY(i.pdf.GetY() + dividedLineHeight)
					if line != "" {
						firstLineDone = true
					}
				}
				i.pdf.SetY(y)
			}
			totalWidth += columnWidth
		}
		i.renderVerticalSpace(verticalSpace)
	}
}

func (i *Invoice) renderTotals(width float64) {
	rightWidth := width / 100 * float64(i.itemHeaders[len(i.itemHeaders)-1].WidthPercentage)
	leftWidth := width - rightWidth
	height := (i.Style.Font.Size / 2) - 1

	i.pdf.CellFormat(leftWidth, height, i.Labels.TotalWithoutTax, "", 0, string(AlignRight), false, 0, "")
	text := i.getSymbolReplacedText(i.TotalWithoutTax)
	i.pdf.CellFormat(rightWidth, height, text, "", 0, string(AlignRight), false, 0, "")
	i.pdf.Ln(-1)

	i.pdf.CellFormat(leftWidth, height, i.Labels.TotalTax, "", 0, string(AlignRight), false, 0, "")
	text = i.getSymbolReplacedText(i.TotalTax)
	i.pdf.CellFormat(rightWidth, height, text, "", 0, string(AlignRight), false, 0, "")
	i.pdf.Ln(-1)

	i.renderVerticalSpace(2)

	totalFont := &Font{i.Style.Font.Name, i.Style.Font.Style, i.Style.Font.Size * 1.3, nil}
	i.setFont(totalFont)
	height = (totalFont.Size / 2) - 1
	i.pdf.CellFormat(leftWidth, height, i.Labels.Total, "", 0, string(AlignRight), false, 0, "")
	text = i.getSymbolReplacedText(i.Total)
	i.pdf.CellFormat(rightWidth, height, text, "", 0, string(AlignRight), false, 0, "")
	i.pdf.Ln(-1)
}

func (i *Invoice) renderVerticalSpace(height float64) {
	i.pdf.Ln(-1)
	i.pdf.Cell(0, height, "")
	i.pdf.Ln(-1)
}

// Little hack to get symbols from another text code page.
func (i *Invoice) getSymbolReplacedText(text string) string {
	tr := i.pdf.UnicodeTranslatorFromDescriptor("cp1252")
	// opletten: tr kan nil zijn als de pdf eerder een fout gegenereerd heeft, bijvoorbeeld als een afbeelding mist.

	if i.pdf.Err() {
		panic(i.pdf.Error())
	}

	text = strings.Replace(text, "€", tr("€"), -1) // unicode 164 symbol is ¤ [Currency]
	text = strings.Replace(text, "®", tr("®"), -1)
	text = strings.Replace(text, "™", tr("™"), -1)
	return text
}

func (i *Invoice) getDateFormat() string {
	if strings.Contains(i.Style.DateFormat, "dd") {
		format := i.Style.DateFormat
		format = strings.Replace(format, "yyyy", "2006", 1)
		format = strings.Replace(format, "MM", "01", 1)
		format = strings.Replace(format, "dd", "02", 1)
		return format
	}
	return i.Style.DateFormat
}
