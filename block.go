package invoices

// block represents an area in a pdf document.
type block struct {
	Text      string
	Font      *Font
	alignment alignment
	border    border
}

// TextAlignment returns the text alignment for this block.
func (b *block) TextAlignment() alignment {
	return b.alignment
}

// SetTextAlignment specifies the text alignment this block.
func (b *block) SetTextAlignment(textalignment alignment) {
	b.alignment = textalignment
}

// Border returns the border setting for this block.
func (b *block) Border() border {
	return b.border
}

// SetBorder specifies the border setting for this block.
func (b *block) SetBorder(borderSetting border) {
	b.border = borderSetting
}
