module gitlab.com/bazzz/invoices

go 1.17

require (
	github.com/google/uuid v1.3.0
	github.com/jung-kurt/gofpdf v1.16.2
	gitlab.com/bazzz/imagebase64 v0.0.0-20220212151726-dbf3a9a5dc8d
	gitlab.com/bazzz/log v0.0.0-20211010075032-ff636a6bfac6
)
